export default {
    API: {
        PORT: 3000
    },
    MONGODB: {
        HOST: 'MongoDB-Container',//'MongoDB-Container',MongoDB-Node01
        PORT: '27017',
        USER_NAME: 'dba-root',//'dba-root',
        USER_PASSWORD: 'mongoadmin',//'mongoadmin',
        DATABASE: 'dbmtwdm'
    },
    TOKEN: {
        EXPIRES: 60*60*4
    }
}
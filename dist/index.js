"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = __importDefault(require("http"));
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const compression_1 = __importDefault(require("compression"));
const mongo_helper_1 = __importDefault(require("./helpers/mongo.helper"));
const socket_logic_1 = __importDefault(require("./sockets/socket.logic"));
const token_helper_1 = __importDefault(require("./helpers/token.helper"));
const test_1 = __importDefault(require("./environments/test"));
///import ENV from './environments/env';
const mongo = mongo_helper_1.default.getInstance(test_1.default.MONGODB, true);
const tokenHelper = token_helper_1.default(test_1.default, mongo);
(() => __awaiter(void 0, void 0, void 0, function* () {
    yield mongo.connect(test_1.default.MONGODB.DATABASE);
    if (mongo.statusConnection.status == 'success') {
        console.log(`Conexión exitosa a MonngoDB en el puerto ${test_1.default.MONGODB.PORT}`);
        const app = express_1.default();
        app.use(express_1.default.json());
        app.use(compression_1.default());
        // app.use(cors({ origin: true, credentials: true }));
        let whitelist = [
            'http://localhost:4200',
            'http://angular.midominio.com/'
        ];
        app.use(cors_1.default({
            origin: (origin, callback) => {
                // allow requests with no origin
                if (!origin)
                    return callback(null, true);
                if (whitelist.indexOf(origin) === -1) {
                    var message = `The CORS policy for this origin doesn't allow access from the particular origin.`;
                    return callback(new Error(message), false);
                }
                return callback(null, true);
            }
        }));
        app.get('/', (req, res) => {
            res.status(200).json({
                ok: true,
                msg: 'API MTWDM Chat funcionando correctamente'
            });
        });
        app.get('/with-cors', (req, res, next) => {
            res.json({ msg: 'WHOAH with CORS it works! 🔝 🎉' });
        });
        app.post('/login', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
            const { correo, apiKey } = req.body;
            console.log('Evaluar REQ.BODY =======>', correo);
            const response = yield mongo.db.collection('usuarios')
                .findOne({ correo, isVerify: false }, { projection: { _id: 0, correo: 1, fotoURL: 1, nombreCompleto: 1 } })
                .then((result) => {
                ///console.log('EVALUAR RESULT =====>', result); 
                if (!result) {
                    return {
                        ok: false,
                        code: 404,
                        msg: `Lo sentimos, el usuario ${correo} no se ha registrado aún o bien no ha habilitado su acceso`
                    };
                }
                return {
                    ok: true,
                    code: 200,
                    msg: `Inicio de sesión realizado de forma exitosa para el usuario ${correo}`,
                    result
                };
            })
                .catch((error) => {
                return {
                    ok: false,
                    code: 500,
                    msg: `Ocurrio un error no contemplado al intentar inicar sesión con el usuario ${correo}`,
                    error
                };
            });
            //console.log('ERROR LOGIN =========>', response);
            if (response.ok == false) {
                res.status(response.code).json(response);
            }
            else {
                // Solicitar Token para usuario
                const token = yield tokenHelper.create(response.result, apiKey);
                res.status(response.code).json(token);
            }
        }));
        const httpServer = http_1.default.createServer(app);
        //const socketIO = require('socket.io')(httpServer);
        const socketIO = require('socket.io')(httpServer, {
            cors: {
                origin: true,
                credentials: true,
            },
            allowEIO3: true
        });
        //app.use(cors({origin: true, credentials: true}));
        let usersList = [];
        const socketLogic = socket_logic_1.default(mongo);
        socketIO.on('connection', (socket) => {
            //TO DO: Logica Chat
            console.log(`Nuevo cliente conectado con ID: ${socket.id}`);
            socketLogic.listenSocketConnect(socket);
            // Logic Register
            socketLogic.register(socket);
            // Logic SignUp
            socketLogic.signUp(socketIO, socket);
            socketLogic.signOut(socketIO, socket);
            // Logic Disconnect
            socketLogic.disconnect(socket);
        });
        httpServer.listen(test_1.default.API.PORT, () => {
            console.log(`Servidor Express funcionando correctamente en puerto ${test_1.default.API.PORT}`);
        });
    }
    else {
        console.log('No se pudo establecer conexión con la base de datos');
        console.log(test_1.default.MONGODB);
        console.log(`mongodb://${test_1.default.MONGODB.USER_NAME}:${test_1.default.MONGODB.USER_PASSWORD}@${test_1.default.MONGODB.HOST}:${test_1.default.MONGODB.PORT}/${test_1.default.MONGODB.DATABASE}`);
    }
}))();
// Handle Errors 
process.on('unhandleRejection', (error, promise) => {
    console.log(`Ocurrio un error no controlado de tipo promise rejection`, promise);
    console.log(`La descripción de error es la siguiente`, error);
    // Close MongoDB
    mongo.close();
    process.exit();
});

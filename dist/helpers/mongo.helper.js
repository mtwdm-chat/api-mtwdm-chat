"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongodb_1 = require("mongodb");
// const connectionString = 'mongodb://mongodb0.example.com:27017,mongodb1.example.com:27018,mongodb2.example.com:27019/admin?replicaSet=mongodb-replicaset';
// "mongodb://dba-root:mongoadmin@MongoDB-Node01:27017,MongoDB-Node01:27018,MongoDB-Node03:27019/dbmtwdm?replicaSet=mongodb-replicaset";
class MongoDBHelper {
    constructor(ENV, isAuth) {
        this.statusConnection = {};
        if (isAuth) {
            this.dbUri = `mongodb://${ENV.USER_NAME}:${ENV.USER_PASSWORD}@${ENV.HOST}:${ENV.PORT}/${ENV.DATABASE}`; // }
        }
        else {
            this.dbUri = `mongodb://${ENV.HOST}:${ENV.PORT}/${ENV.DATABASE}`;
        }
        //this.dbUri = "mongodb://dba-root:mongoadmin@MongoDB-Node01:27017,MongoDB-Node02:27018,MongoDB-Node03:27019?replicaSet=mongodb-replicaset";
        console.log("Valor dbUri: ", this.dbUri);
    }
    static getInstance(ENV, isAuth = false) {
        return this._instance || (this._instance = new this(ENV, isAuth));
    }
    connect(dataBase, options = { useNewUrlParser: true, useUnifiedTopology: true }) {
        return __awaiter(this, void 0, void 0, function* () {
            this.statusConnection = yield mongodb_1.MongoClient.connect(this.dbUri, options)
                .then((cnn) => {
                return {
                    status: 'success',
                    conexion: cnn,
                    msg: `Servidor MongoDB corriendo de forma exitosa!!!`
                };
            })
                .catch((error) => {
                console.log(error);
                return {
                    status: 'error',
                    error,
                    msg: 'Ocurrio un error al intentar establecer conexion con el servidor de Mongo'
                };
            });
            if (this.statusConnection.status == 'success') {
                this.cnn = this.statusConnection.conexion;
                this.db = this.cnn.db(dataBase);
            }
            else {
                this.cnn = null;
                this.db = null;
            }
        });
    }
    setDataBase(database) {
        this.db = this.cnn.db(database);
    }
    close() {
        if (this.cnn != null) {
            this.cnn.close();
        }
    }
}
exports.default = MongoDBHelper;

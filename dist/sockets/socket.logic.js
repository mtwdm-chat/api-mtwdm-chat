"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
//const mongo = MongoHelper.getInstance(ENV.MONGODB);
let usersList = [];
exports.default = (mongo) => {
    return {
        listenSocketConnect: (socket) => __awaiter(void 0, void 0, void 0, function* () {
            yield mongo.db.collection('sockets')
                .insertOne({
                socketId: socket.id,
                usuario: null
            })
                .then((result) => { } /*console.log(result)*/)
                .catch((error) => console.log(error));
        }),
        register: (socket) => __awaiter(void 0, void 0, void 0, function* () {
            //console.log(io);
            socket.on('register', (payload) => __awaiter(void 0, void 0, void 0, function* () {
                // Guardar en Base de Datos
                console.log(`Escuchando mensaje Register`, payload);
                yield mongo.db.collection('sockets')
                    .findOneAndUpdate({ socketId: socket.id }, {
                    $set: { usuario: payload.email }
                })
                    .then((result) => { } /*console.log(result)*/)
                    .catch((error) => console.log(error));
                yield mongo.db.collection('usuarios').findOneAndUpdate({ correo: payload.email }, // Criterio de Busqueda
                {
                    $setOnInsert: {
                        isVerify: false
                    },
                    $set: {
                        nombreCompleto: payload.fullName,
                        fotoURL: payload.photoUrl,
                        isConnected: false
                    }
                }, {
                    upsert: true
                })
                    .then((result) => __awaiter(void 0, void 0, void 0, function* () {
                    //console.log(result);                    
                }))
                    .catch((error) => console.log(error));
            }));
        }),
        signUp: (io, socket) => {
            //console.log(io);
            socket.on('signUp', (payload) => __awaiter(void 0, void 0, void 0, function* () {
                // Guardar en Base de Datos
                console.log(`Escuchando mensaje signUp`, payload);
                yield mongo.db.collection('sockets')
                    .findOneAndUpdate({ socketId: socket.id }, {
                    $set: { usuario: payload.email }
                })
                    .then((result) => { } /*console.log(result)*/)
                    .catch((error) => console.log(error));
                yield mongo.db.collection('usuarios').findOneAndUpdate({ correo: payload.email }, // Criterio de Busqueda
                {
                    $setOnInsert: {
                        isVerify: false
                    },
                    $set: {
                        nombreCompleto: payload.fullName,
                        fotoURL: payload.photoUrl,
                        isConnected: true
                    }
                }, {
                    upsert: true
                })
                    .then((result) => __awaiter(void 0, void 0, void 0, function* () {
                    //console.log(result);                    
                }))
                    .catch((error) => console.log(error));
                let cursor = yield mongo.db.collection('usuarios').find({ isConnected: true }).toArray();
                usersList.push(cursor);
                // Retransmitir la variable payload  a todos los clientes conectados
                io.emit('broadcast-message', cursor);
            }));
        },
        signOut: (io, socket) => {
            //console.log(io);
            socket.on('signOut', (payload) => __awaiter(void 0, void 0, void 0, function* () {
                // Guardar en Base de Datos
                console.log(`Escuchando mensaje SingOut`, payload);
                yield mongo.db.collection('usuarios')
                    .findOneAndUpdate({ correo: payload.email }, {
                    $set: { isConnected: false }
                })
                    .then((result) => { } /*console.log(result)*/)
                    .catch((error) => console.log(error));
                //Eliminar Socket desconectado
                yield mongo.db.collection('sockets')
                    .deleteOne({ socketId: socket.id })
                    .then((result) => { } /*console.log(result)*/)
                    .catch((error) => console.log(error));
                let cursor = yield mongo.db.collection('usuarios').find({ isConnected: true }).toArray();
                usersList.push(cursor);
                // Retransmitir la variable payload  a todos los clientes conectados
                io.emit('broadcast-message', cursor);
            }));
        },
        disconnect: (socket) => {
            socket.on('disconnect', () => __awaiter(void 0, void 0, void 0, function* () {
                console.log(`Desconexión del cliente con ID: ${socket.id}`);
                let cursor = yield mongo.db.collection('sockets').findOne({ socketId: socket.id });
                if (cursor) {
                    console.log(`Cursor: ${cursor.usuario}`);
                    yield mongo.db.collection('usuarios')
                        .findOneAndUpdate({ correo: cursor.usuario }, {
                        $set: { isConnected: false }
                    })
                        .then((result) => { } /*console.log(result)*/)
                        .catch((error) => console.log(error));
                }
                //Eliminar Socket desconectado
                yield mongo.db.collection('sockets')
                    .deleteOne({ socketId: socket.id })
                    .then((result) => { } /*console.log(result)*/)
                    .catch((error) => console.log(error));
                let cursor2 = yield mongo.db.collection('usuarios').find({ isConnected: true }).toArray();
                usersList.push(cursor2);
                ///console.log(`Curso2r: ${cursor2}`); 
                // Retransmitir la variable payload  a todos los clientes conectados
                socket.emit('broadcast-message', cursor2);
                // TO DO: Guardar Log en Base de Datos
            }));
        }
    };
};

import http from 'http';
import express, { Request, Response } from 'express';
import cors from 'cors';
import compression from 'compression';
import { Socket } from 'socket.io';
import MongoDBHelper from './helpers/mongo.helper';
import SocketLogic from './sockets/socket.logic';
import TokenHelper from './helpers/token.helper';

import ENV from './environments/test';
///import ENV from './environments/env';

const mongo = MongoDBHelper.getInstance(ENV.MONGODB, true);
const tokenHelper = TokenHelper(ENV, mongo);

(async () => {
  await mongo.connect(ENV.MONGODB.DATABASE);
  if (mongo.statusConnection.status == 'success') {
    console.log(`Conexión exitosa a MonngoDB en el puerto ${ENV.MONGODB.PORT}`);

    const app = express();
    app.use(express.json());
    app.use(compression());

    // app.use(cors({ origin: true, credentials: true }));
    let whitelist = [
      'http://localhost:4200',
      'http://angular.midominio.com/'
    ];

    app.use(cors({
      origin: (origin, callback) => {
        // allow requests with no origin
        if (!origin) return callback(null, true);
        if (whitelist.indexOf(origin) === -1) {
          var message = `The CORS policy for this origin doesn't allow access from the particular origin.`;
          return callback(new Error(message), false);
        }
        return callback(null, true);
      }
    }));

    app.get('/', (req: Request, res: Response) => {
      res.status(200).json({
        ok: true,
        msg: 'API MTWDM Chat funcionando correctamente'
      });
    });

    app.get('/with-cors', (req, res, next) => {
      res.json({ msg: 'WHOAH with CORS it works! 🔝 🎉' })
    })

    app.post('/login', async (req: Request, res: Response) => {

      const { correo, apiKey } = req.body;

      console.log('Evaluar REQ.BODY =======>', correo);

      const response: any = await mongo.db.collection('usuarios')
        .findOne(
          { correo, isVerify: false },
          { projection: { _id: 0, correo: 1, fotoURL: 1, nombreCompleto: 1 } }
        )
        .then((result: any) => {

          ///console.log('EVALUAR RESULT =====>', result); 

          if (!result) {
            return {
              ok: false,
              code: 404,
              msg: `Lo sentimos, el usuario ${correo} no se ha registrado aún o bien no ha habilitado su acceso`
            }
          }
          return {
            ok: true,
            code: 200,
            msg: `Inicio de sesión realizado de forma exitosa para el usuario ${correo}`,
            result
          }
        })
        .catch((error: any) => {
          return {
            ok: false,
            code: 500,
            msg: `Ocurrio un error no contemplado al intentar inicar sesión con el usuario ${correo}`,
            error
          }
        });


      //console.log('ERROR LOGIN =========>', response);

      if (response.ok == false) {
        res.status(response.code).json(response);
      } else {
        // Solicitar Token para usuario
        const token: any = await tokenHelper.create(response.result, apiKey);
        res.status(response.code).json(token);
      }


    })


    const httpServer = http.createServer(app);
    //const socketIO = require('socket.io')(httpServer);
    const socketIO = require('socket.io')(httpServer, {
      cors: {
        origin: true,//"http://angular.midominio.com/#/login",///"http://localhost:4200",
        credentials: true,
      },
      allowEIO3: true
    });


    //app.use(cors({origin: true, credentials: true}));

    let usersList: any[] = [];
    const socketLogic = SocketLogic(mongo);
    socketIO.on('connection', (socket: Socket) => {
      //TO DO: Logica Chat
      console.log(`Nuevo cliente conectado con ID: ${socket.id}`);

      socketLogic.listenSocketConnect(socket);
      // Logic Register
      socketLogic.register(socket);
      // Logic SignUp
      socketLogic.signUp(socketIO, socket);

      socketLogic.signOut(socketIO, socket);
      // Logic Disconnect
      socketLogic.disconnect(socket);
    });




    httpServer.listen(ENV.API.PORT, () => {
      console.log(`Servidor Express funcionando correctamente en puerto ${ENV.API.PORT}`);
    });
  }
  else {
    console.log('No se pudo establecer conexión con la base de datos');
    console.log(ENV.MONGODB)
    console.log(`mongodb://${ENV.MONGODB.USER_NAME}:${ENV.MONGODB.USER_PASSWORD}@${ENV.MONGODB.HOST}:${ENV.MONGODB.PORT}/${ENV.MONGODB.DATABASE}`);

  }
})();

// Handle Errors 
process.on('unhandleRejection', (error: any, promise) => {
  console.log(`Ocurrio un error no controlado de tipo promise rejection`, promise);
  console.log(`La descripción de error es la siguiente`, error);
  // Close MongoDB
  mongo.close();
  process.exit();
});


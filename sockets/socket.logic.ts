import { Socket } from "socket.io";
import TokenHelper from '../helpers/token.helper';
import ENV from '../environments/env';
import MongoHelper from '../helpers/mongo.helper';

//const mongo = MongoHelper.getInstance(ENV.MONGODB);


let usersList: any[] = [];
export default (mongo: any) => {

    return {
        listenSocketConnect: async (socket: Socket) => {
            await mongo.db.collection('sockets')
                .insertOne({
                    socketId: socket.id,
                    usuario: null
                })
                .then((result: any) => { }/*console.log(result)*/)
                .catch((error: any) => console.log(error));
        },
        register: async(socket: Socket) => {
            //console.log(io);
            socket.on('register', async (payload: any) => {
                // Guardar en Base de Datos
                console.log(`Escuchando mensaje Register`, payload);

                await mongo.db.collection('sockets')
                    .findOneAndUpdate(
                        { socketId: socket.id },
                        {
                            $set: { usuario: payload.email }
                        }
                    )
                    .then((result: any) => { }/*console.log(result)*/)
                    .catch((error: any) => console.log(error)
                    );


                await mongo.db.collection('usuarios').findOneAndUpdate(
                        { correo: payload.email }, // Criterio de Busqueda
                        {
                            $setOnInsert: {
                                isVerify: false
                            },
                            $set: {
                                nombreCompleto: payload.fullName,
                                fotoURL: payload.photoUrl,
                                isConnected: false
                            }
                        },
                        {
                            upsert: true
                        }
                    )
                    .then(async (result: any) => {
                        //console.log(result);                    
                    })
                    .catch((error: any) => console.log(error)
                    );
            
            });

        },
        signUp: (io: any, socket: Socket) => {
            //console.log(io);
            socket.on('signUp', async (payload: any) => {
                // Guardar en Base de Datos
                console.log(`Escuchando mensaje signUp`, payload);

                await mongo.db.collection('sockets')
                    .findOneAndUpdate(
                        { socketId: socket.id },
                        {
                            $set: { usuario: payload.email }
                        }
                    )
                    .then((result: any) => { }/*console.log(result)*/)
                    .catch((error: any) => console.log(error)
                    );


                await mongo.db.collection('usuarios').findOneAndUpdate(
                        { correo: payload.email }, // Criterio de Busqueda
                        {
                            $setOnInsert: {
                                isVerify: false
                            },
                            $set: {
                                nombreCompleto: payload.fullName,
                                fotoURL: payload.photoUrl,
                                isConnected: true
                            }
                        },
                        {
                            upsert: true
                        }
                    )
                    .then(async (result: any) => {
                        //console.log(result);                    
                    })
                    .catch((error: any) => console.log(error)
                    );

                    let cursor = await mongo.db.collection('usuarios').find({isConnected: true}).toArray();                    
                    usersList.push(cursor);  
                
                

                // Retransmitir la variable payload  a todos los clientes conectados
                io.emit('broadcast-message', cursor);
            });
        },
        signOut: (io: any, socket: Socket) => {
            //console.log(io);
            socket.on('signOut', async (payload: any) => {
                // Guardar en Base de Datos
                console.log(`Escuchando mensaje SingOut`, payload);               

                await mongo.db.collection('usuarios')
                    .findOneAndUpdate(
                        { correo: payload.email },
                        {
                            $set: { isConnected: false }
                        }
                    )
                    .then((result: any) => { }/*console.log(result)*/)
                    .catch((error: any) => console.log(error)
                    );
                
                 //Eliminar Socket desconectado
                 await mongo.db.collection('sockets')
                    .deleteOne({ socketId: socket.id })
                    .then((result: any) => { }/*console.log(result)*/)
                    .catch((error: any) => console.log(error)
                );
                
                let cursor = await mongo.db.collection('usuarios').find({isConnected: true}).toArray();

                usersList.push(cursor);  

                // Retransmitir la variable payload  a todos los clientes conectados
                io.emit('broadcast-message', cursor);
            });
        },
        disconnect: (socket: Socket) => {
            socket.on('disconnect', async () => {
                console.log(`Desconexión del cliente con ID: ${socket.id}`);

                let cursor = await mongo.db.collection('sockets').findOne({socketId: socket.id});                

                if(cursor){
                    console.log(`Cursor: ${cursor.usuario}`);
                    
                    await mongo.db.collection('usuarios')
                        .findOneAndUpdate(
                            { correo: cursor.usuario },
                            {
                                $set: { isConnected: false }
                            }
                        )
                        .then((result: any) => { }/*console.log(result)*/)
                        .catch((error: any) => console.log(error)
                    );
                }
                

                //Eliminar Socket desconectado
                await mongo.db.collection('sockets')
                    .deleteOne({ socketId: socket.id })
                    .then((result: any) => { }/*console.log(result)*/)
                    .catch((error: any) => console.log(error)
                    );

                
                let cursor2 = await mongo.db.collection('usuarios').find({isConnected: true}).toArray();

                usersList.push(cursor2); 
                ///console.log(`Curso2r: ${cursor2}`); 

                // Retransmitir la variable payload  a todos los clientes conectados
                socket.emit('broadcast-message', cursor2);


                // TO DO: Guardar Log en Base de Datos
            });
        }
    }
};
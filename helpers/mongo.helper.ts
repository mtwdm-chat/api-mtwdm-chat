import { MongoClient, MongoClientOptions } from 'mongodb';

// const connectionString = 'mongodb://mongodb0.example.com:27017,mongodb1.example.com:27018,mongodb2.example.com:27019/admin?replicaSet=mongodb-replicaset';
// "mongodb://dba-root:mongoadmin@MongoDB-Node01:27017,MongoDB-Node01:27018,MongoDB-Node03:27019/dbmtwdm?replicaSet=mongodb-replicaset";


export default class MongoDBHelper {

    public db: any;
    public statusConnection: any = {};

    private static _instance: MongoDBHelper;
    private cnn: any;
    private dbUri: string;

    

    constructor(ENV: any, isAuth: boolean) {
        if (isAuth) {
            this.dbUri = `mongodb://${ENV.USER_NAME}:${ENV.USER_PASSWORD}@${ENV.HOST}:${ENV.PORT}/${ENV.DATABASE}`;// }

        } else {
            this.dbUri = `mongodb://${ENV.HOST}:${ENV.PORT}/${ENV.DATABASE}`;
        }

        //this.dbUri = "mongodb://dba-root:mongoadmin@MongoDB-Node01:27017,MongoDB-Node02:27018,MongoDB-Node03:27019?replicaSet=mongodb-replicaset";

        console.log("Valor dbUri: ", this.dbUri)
    }

    public static getInstance(ENV: any, isAuth: boolean = false) {
        return this._instance || (this._instance = new this(ENV, isAuth));
    }

    public async connect(dataBase: string, options: MongoClientOptions = { useNewUrlParser: true, useUnifiedTopology: true }) {
        this.statusConnection = await MongoClient.connect(this.dbUri, options)
            .then((cnn: any) => {
                return {
                    status: 'success',
                    conexion: cnn,
                    msg: `Servidor MongoDB corriendo de forma exitosa!!!`
                }
            })
            .catch((error: any) => {
                console.log(error);
                return {
                    status: 'error',
                    error,
                    msg: 'Ocurrio un error al intentar establecer conexion con el servidor de Mongo'
                }
            })

        if (this.statusConnection.status == 'success') {
            this.cnn = this.statusConnection.conexion;
            this.db = this.cnn.db(dataBase);
        } else {
            this.cnn = null;
            this.db = null;
        }
    }

    public setDataBase(database: string) {
        this.db = this.cnn.db(database);
    }

    public close() {
        if (this.cnn != null) {
            this.cnn.close();
        }
    }




}